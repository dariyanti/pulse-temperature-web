<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dummy extends CI_Controller {

	public function __construct ()
	{
		parent::__construct();
		$this->halaman->sebar('DummyMenu', true); // set aktif navbar menu (kiri) dummy data
	}
	 
	public function index()
	{
		$pasiens = $this->db->get('pasien')->result(); // ambil dari list pasien
		echo $this->halaman->tampil('website.dummy.index',compact('pasiens')); // kirimkan ke views -> website -> dummy -> index.blade.php
	}

	public function postView(){
		echo $this->halaman->tampil('website.dummy.post'); // halaman untuk melakukan post data dummy ke server
	}

	public function store() // fungsi buat melakukan random post
	{
		$batch  = [];
		for ($i=1; $i <= 10 ; $i++) { // buat 10 data sesuai banyak data di list pasien
			$detak = rand(80,160); // random antara 80 sampai 160
			$suhu = rand(30,35); // random antara 30 sampai 35
			$data = array(
				'pasien_id' => $i, // pasied id
				'jam' => Date('Y-m-d h:i:s'), // waktu server
				'detak' => $detak, // detak nya tadi
				'suhu' => $suhu // suhu nya tadi
			);
			array_push($batch,$data); // dimasukkan ke array
		}
        $update = $this->db->insert_batch('data_pasien',$batch); // disimpen langsung arraynya
        echo json_encode(array("status" => "success","data" => $batch));
	}

	public function get($id)
	{
		$data = $this->db->select('*')->from('data_pasien')->where('pasien_id',$id)->order_by('jam','DESC')->get(); // ambil data sesuai id pasien
        echo json_encode($data->row()); // kirimkan ke view nya
	}

	public function detail($id){
		$pasien = $this->db->select('*')->from('pasien')->where('id',$id)->get()->row(); // ambil id pasien sesuai yang di klik dari view
		echo $this->halaman->tampil('website.dummy.detail',compact('pasien')); // kirimkan data pasien ke view detail pasien, view -> website -> dummy -> detail
	}
}

