<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	public function postData()
	{
        $detak = $this->input->get('detak');
        $suhu = $this->input->get('suhu');
        $data = array(
            'jam' => Date('Y-m-d h:i:s'),
            'detak' => $detak,
            'suhu' => $suhu
        );
        $update = $this->db->insert('test',$data);
        if($update){
            echo "success";
        }else{
            echo "fail";
        }
        // echo json_encode(array("status" => "success","detak" => $detak,"suhu" => $suhu));
    }

    public function temperature(){
        $data = $this->db->select('jam, detak, suhu')->from('data_pasien')->where('pasien_id',1)->order_by('jam','DESC')->get();
        echo json_encode($data->row());
    }

    
    public function testView()
    {
        echo $this->halaman->tampil('website.test.index');
    }

    public function testData()
    {
        echo $this->halaman->tampil('website.test.post');
    }
}

