<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Real extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->halaman->sebar('RealMenu', true); // set aktif navbar menu (kiri) real
	}
	
	public function index()
	{
		echo $this->halaman->tampil('website.real.index'); // tampilkan halaman utama, views -> website -> real -> index.blade.php
	}

	public function android()
	{
		echo $this->halaman->tampil('website.android.index'); // tampilkan halaman utama buat android, views -> website -> android -> index.blade.php
	}

	public function postData() // ini yang dari alat
	{
		$detak = $this->input->get('detak'); // ambil detak dari alat
		$suhu = $this->input->get('suhu'); // ambil suhu dari alat
		$data = array(
			'jam' => Date('Y-m-d h:i:s'),
			'detak' => $detak,
			'suhu' => $suhu,
			'pasien_id' => 1 // kita pake pasien id nya 1, kalau mau diganti silakan, tapi tambahi juga pasiennya
		);
		$update = $this->db->insert('data_pasien', $data); // masukkan ke database
		if ($update) {
			echo "success"; // kirim lagi ke arduino + ESP
		} else {
			echo "fail";
		}
	}

	public function getData()
	{
		$data = $this->db->select('jam, detak, suhu')->from('data_pasien')->where('pasien_id', 1)->order_by('jam', 'DESC')->get(); // ambil pasien dengan id 1, bisa diganti sesuai dengan id yang postData diatas
		echo json_encode($data->row());
	}
}
