<!DOCTYPE html>
<html lang="en">

<head>
    @include('website.parsial.metadata')
    @include('website.parsial.styles')
</head>

<body class="hold-transition skin-pink fixed">
    @include('website.parsial.content-android')
    @include('website.parsial.scripts')
</body>

</html>