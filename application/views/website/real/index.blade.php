@extends('website.layout')
@section('title', 'Real Time')
@section('content')

<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Real Time Data
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="#">Real Time View</a></li>
    </ol>
  </section>
  <section class="content">

    <!-- Main content -->
    <div class="row">
      <div class="col-xs-12">
        <!-- interactive chart -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-bar-chart-o"></i>

            <h3 class="box-title">Interactive Area Chart</h3>

            <div class="box-tools pull-right">
              Real time
            </div>
          </div>
          <div class="box-body">
            <div id="interactive" style="height: 500px;"></div>
          </div>
          <!-- /.box-body-->
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
</section>
@endsection
@section('scripts')
<!-- FLOT CHARTS -->
<script type="text/javascript" src="{{ base_url('website/plugins/Flot/jquery.js') }}"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="{{ base_url('website/plugins/Flot/excanvas.min.js')}}"></script><![endif]-->
<script type="text/javascript" src="{{ base_url('website/plugins/Flot/jquery.flot.js') }}"></script>
<script type="text/javascript" src="{{ base_url('website/plugins/Flot/jquery.flot.time.js') }}"></script>
<script type="text/javascript" src="{{ base_url('website/plugins/Flot/jquery.flot.symbol.js') }}"></script>
<!-- Page script -->
<script>
  let $Flot = jQuery.noConflict();

    let detak = [], suhu = []; // pertama definisikan variabel buat menampung detak dan suhu
    let dataset;
    let totalPoints = 100; // panjang dari grafik yang hendak direfresh
    let updateInterval = 1000; // 1000 milisecond alias 1 detik
    let temp, jam;

    let options = { // ini buat options flot nya
        series: {
            lines: {
                lineWidth: 1.2 // tebal line  yang pertama
            },
            lines: {
                lineWidth: 1.2 // tebal line yang kedua
            }
        },
        xaxis: { // yang bawah
            mode: "time", // mode time
            tickSize: [1, "second"], // tick nya per 1 detik
            tickFormatter: function (v, axis) {
                let date = new Date(v);

                if (date.getSeconds() % 20 == 0) { // setiap 20 detik sekali dimunculkan jam nya
                    let hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
                    let minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
                    let seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

                    return hours + ":" + minutes + ":" + seconds;
                } else {
                    return "";
                }
            },
            axisLabel: "Time",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 10
        },
        yaxes: [ // yang tegak
            {
                min: 50, // nilai minimal
                max: 200, // nilai maksimal
                tickSize: 5,
                tickFormatter: function (v, axis) {
                    if (v % 5 == 0) { // dbuat ngeprint bpm nya
                        return v + " BPM";
                    } else {
                        return "";
                    }
                },
                axisLabel: "detak loading",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 6
            }, {
                min: 20,
                max: 50,
                tickSize: 5,
                tickFormatter: function (v, axis) {
                    if (v % 5 == 0) { // dibuat ngeprint suhu nya
                        return v + " &#8451;";
                    } else {
                        return "";
                    }
                },
                position: "right",
                axisLabel: "suhu",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 6
            }
        ],
        legend: {
            position:"nw"
        },
        grid: {      
            backgroundColor: { colors: ["#ffffff", "#EDF5FF"] }
        }
    };

    function initData() { // awal kali dijalankan pasti ini, dia akan buat 100 point kosong dulu
        for (let i = 0; i < totalPoints; i++) {
            let temp = ["12:57:48", 0];

            detak.push(temp);
            suhu.push(temp);
        }
    }

    function GetData() { // ngambil data
        $Flot.ajaxSetup({ cache: false });

        $Flot.ajax({
            url: "<?php echo base_url('Real/getData')?>", // ngambil datanya disini, akan nyambung di controller Real/getData
            dataType: 'json',
            success: update, // fungsi update dibawah
            error: function () {
                setTimeout(GetData, updateInterval); // kalau error ditunggu 1 detik
            }
        });
    }

    function update(_data) {
        detak.shift(); // kosongi datanya
        suhu.shift(); // kosongi datanya
        console.log(_data)
        jam = new Date(_data.jam).getTime() // ambil waktu nya 

        temp = [jam, _data.detak]; // ambil detaknya, dibuat bentuk array dengan jam biar senada
        detak.push(temp); // di taruh ke array detak tadi
        temp = [jam, _data.suhu]; // ini juga sama kayak detak
        suhu.push(temp); // di taruh ke array suhu
        dataset = [
            { label: "Detak:" + _data.detak + " BPM", data: detak, lines: { lineWidth: 1.2 }, color: "#00FF00" }, // ini yang di pojok kiri atas dari grafik
            { label: "Suhu:" + _data.suhu + " &#8451;", data: suhu, lines: { lineWidth: 1.2 }, color: "#FF0000", yaxis :2 }, // ini yang di pojok kiri atas dari grafik
        ];

        $Flot.plot($Flot("#interactive"), dataset, options); // digambar deh
        setTimeout(GetData, updateInterval); // nunggu 1 detik lagi buat update
    }

    $Flot(document).ready(function () { // digunakan jika halaman udah direload
        initData(); // maka dipanggil ini dulu

        dataset = [        
            { label: "Detak:", data: detak, lines: { lineWidth:1.2 }, color: "#00FF00" }, // ini yang di pojok kiri atas
            { label: "Suhu:", data: suhu, lines: { lineWidth: 1.2 }, color: "#FF0000", yaxis :2 }, // ini yang di pojok kiri atas
        ];

        $Flot.plot($Flot("#interactive"), dataset, options); // gambar grafiknya
        setTimeout(GetData, updateInterval); // nunggu 1 detik lagi buat update
    });


</script>

@endsection