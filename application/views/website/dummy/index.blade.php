@extends('website.layout')
@section('title', 'Dummy Data')
@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
          DATA PASIEN
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Dummy Data</a></li>
            <li class="active">Data Pasien</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            @php
                $id_pasien = [];
            @endphp
            @foreach ($pasiens as $pasien)
            <div class="col-md-3">
                <div class="box box-warning box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">PASIEN {{ $pasien->id }}</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body pasien-{{ $pasien->id }}">
                        <p class="font-18 detak-{{ $pasien->id }}"></p>
                        <p class="font-18 suhu-{{ $pasien->id }}"></p>
                        <p class="font-18">Status: <span class="status-{{ $pasien->id }}"></span></p>
                        <a class="btn btn-primary" href="{{ base_url('Dummy/detail/').$pasien->id }}">Detail</a>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                @php
                    array_push($id_pasien,$pasien->id);
                @endphp
            </div>
            @endforeach
        </div>
    </section>
</div>
@endsection
@section('styles')
<style>
    .font-18 {
        font-size: 18px;
    }

    .color-red {
        color: red;
    }
</style>
@endsection
@section('scripts')
<script>
    function getData() {  
        let pasien = '<?php echo json_encode($id_pasien)?>'
        pasien = JSON.parse(pasien)
        $.each(pasien, function (i,v) { 
            $.ajax({
                type: "GET",
                url: "<?php echo base_url('Dummy/get/')?>" + v,
                dataType: "JSON",
                success: function (response) {
                    console.log(response)
                    $('.status-'+response.pasien_id).removeClass('text-red');
                    $('.detak-' + response.pasien_id).html('Detak : '+response.detak+' <i class="fa fa-heart color-red"></i>')
                    $('.suhu-' + response.pasien_id).html('Suhu : '+response.suhu+' &#8451;')
                    if(response.detak < 90){
                        $('.status-' + response.pasien_id).html('<b>Hipotensi</b>')
                        $('.status-'+response.pasien_id).addClass('text-red');
                    }else if(response.detak > 140){
                        $('.status-' + response.pasien_id).html('<b>Hipertensi</b>')
                        $('.status-'+response.pasien_id).addClass('text-red');
                    }else{
                        $('.status-' + response.pasien_id).html('Normal')
                    }
                }
            });
        });
    }
    $(document).ready(function () {
        setInterval(getData,1000);
    });
    
</script>
@endsection