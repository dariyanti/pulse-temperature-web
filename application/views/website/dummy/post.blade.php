@extends('website.layout')
@section('title', 'Real Time')
@section('content')

<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Test Real Time Post Data
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="#">Test Real Time</a></li>
        </ol>
    </section>
    <section class="content">

        <!-- Main content -->
        <div class="row">
            <div class="col-xs-12">
                <!-- interactive chart -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="fa fa-bar-chart-o"></i>

                        <h3 class="box-title">Status Data</h3>

                        <div class="box-tools pull-right">
                            Real time
                        </div>
                    </div>
                    <div class="box-body">

                    </div>
                    <!-- /.box-body-->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.col -->
</div>
<!-- /.row -->
</section>
@endsection
@section('scripts')
<!-- Page script -->
<script>
    function postData(){
        $.ajax({
            type: "GET",
            url: "<?php echo base_url('Dummy/store')?>",
            dataType: "JSON",
            success: function (response) {
                let html = ''
                data = response.data
                $.each(data, function (i,v) {
                    html += '<div class="col-md-3"><div class="small-box bg-green"><div class="inner"><p>Data Dikirim</p><p>Pasien ID : '+v.pasien_id+'</p> <p>Detak : '+v.detak+'</p><p>Suhu : '+v.suhu+'</p><p>Status : '+response.status+'</p></div></div></div>'
                });
                $('.box-body').html(html)
            }
        });
    }

    $(document).ready(function () {
        setInterval(postData, 1000);
    });

</script>

@endsection