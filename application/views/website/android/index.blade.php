@extends('website.layout-android')
@section('title', 'Real Time')
@section('content')

<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Real Time Data
        </h1>
    </section>
    <section class="content">

        <!-- Main content -->
        <div class="row">
            <div class="col-xs-12">
                <!-- interactive chart -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="fa fa-bar-chart-o"></i>

                        <h3 class="box-title">Interactive Area Chart</h3>

                        <div class="box-tools pull-right">
                            Real time
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <h1 class="text-center">DETAK JANTUNG</h1>
                                <h1 class="text-center detak"></h1>
                            </div>
                            <div class="col-md-4">
                                <h1 class="text-center">SUHU</h1>
                                <h1 class="text-center suhu"></h1>
                            </div>
                            <div class="col-md-4">
                                <h1 class="text-center">STATUS</h1>
                                <h1 class="text-center status"></h1>
                            </div>
                        </div>
                        <div id="interactive" style="height: 300px;"></div>
                    </div>
                    <!-- /.box-body-->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.col -->
</div>
<!-- /.row -->
</section>
@endsection
@section('styles')
<style>
    .color-red {
        color: red;
    }
</style>
@endsection
@section('scripts')
<!-- FLOT CHARTS -->
<script type="text/javascript" src="{{ base_url('website/plugins/Flot/jquery.js') }}"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="{{ base_url('website/plugins/Flot/excanvas.min.js')}}"></script><![endif]-->
<script type="text/javascript" src="{{ base_url('website/plugins/Flot/jquery.flot.js') }}"></script>
<script type="text/javascript" src="{{ base_url('website/plugins/Flot/jquery.flot.time.js') }}"></script>
<script type="text/javascript" src="{{ base_url('website/plugins/Flot/jquery.flot.symbol.js') }}"></script>
<!-- Page script -->
<script>
    let $Flot = jQuery.noConflict();

    let detak = [], suhu = [];
    let dataset;
    let totalPoints = 100;
    let updateInterval = 1000;
    let temp, jam;

    let options = {
        series: {
            lines: {
                lineWidth: 1.2
            },
            lines: {
                lineWidth: 1.2
            }
        },
        xaxis: {
            mode: "time",
            tickSize: [1, "second"],
            tickFormatter: function (v, axis) {
                let date = new Date(v);

                if (date.getSeconds() % 20 == 0) {
                    let hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
                    let minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
                    let seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

                    return hours + ":" + minutes + ":" + seconds;
                } else {
                    return "";
                }
            },
            axisLabel: "Time",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 10
        },
        yaxes: [
            {
                min: 50,
                max: 200,
                tickSize: 5,
                tickFormatter: function (v, axis) {
                    if (v % 5 == 0) {
                        return v + " BPM";
                    } else {
                        return "";
                    }
                },
                axisLabel: "detak loading",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 6
            }, {
                min: 20,
                max: 50,
                tickSize: 5,
                tickFormatter: function (v, axis) {
                    if (v % 5 == 0) {
                        return v + " &#8451;";
                    } else {
                        return "";
                    }
                },
                position: "right",
                axisLabel: "suhu",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 6
            }
        ],
        legend: {
            position:"nw"
        },
        grid: {      
            backgroundColor: { colors: ["#ffffff", "#EDF5FF"] }
        }
    };

    function initData() {
        for (let i = 0; i < totalPoints; i++) {
            let temp = ["12:57:48", 0];

            detak.push(temp);
            suhu.push(temp);
        }
    }

    function GetData() {
        $Flot.ajaxSetup({ cache: false });

        $Flot.ajax({
            url: "<?php echo base_url('Real/getData')?>",
            dataType: 'json',
            success: update,
            error: function () {
                setTimeout(GetData, updateInterval);
            }
        });
    }

    function update(_data) {
        detak.shift();
        suhu.shift();
        console.log(_data)
        jam = new Date(_data.jam).getTime()
        $('.status').removeClass('text-red');
        $('.detak').html(' <i class="fa fa-heart color-red"></i> '+_data.detak+' BPM')
        $('.suhu').html(_data.suhu+' &#8451;')
        if(_data.detak < 90){ 
            $('.status').html('<b>Hipotensi</b>')
            $('.status').addClass('text-red')
        }else if(_data.detak > 140){
            $('.status').html('<b>Hipertensi</b>')
            $('.status').addClass('text-red')
        }else{
            $('.status').html('Normal')
        }
        temp = [jam, _data.detak];
        detak.push(temp);
        temp = [jam, _data.suhu];
        suhu.push(temp);
        dataset = [
            { label: "Detak:" + _data.detak + " BPM", data: detak, lines: { lineWidth: 1.2 }, color: "#00FF00" },
            { label: "Suhu:" + _data.suhu + " &#8451;", data: suhu, lines: { lineWidth: 1.2 }, color: "#FF0000", yaxis :2 },
        ];

        $Flot.plot($Flot("#interactive"), dataset, options);
        setTimeout(GetData, updateInterval);
    }

    $Flot(document).ready(function () {
        initData();

        dataset = [        
            { label: "Detak:", data: detak, lines: { lineWidth:1.2 }, color: "#00FF00" },
            { label: "Suhu:", data: suhu, lines: { lineWidth: 1.2 }, color: "#FF0000", yaxis :2 },
        ];

        $Flot.plot($Flot("#interactive"), dataset, options);
        setTimeout(GetData, updateInterval);
    });


</script>

@endsection