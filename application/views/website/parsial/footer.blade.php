<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>TA</b> Hardware
  </div>
  <strong>Copyright &copy; 2019<a href="{{ base_url() }}">Teknik Informatika</a>.</strong> All rights
  reserved.
</footer>