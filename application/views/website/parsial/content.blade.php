<div class="wrapper">
    @include('website.parsial.navbar')
    @include('website.parsial.sidebar')
    @yield('content')
    @include('website.parsial.footer')
</div>
