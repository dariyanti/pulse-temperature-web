 <!-- Left side column. contains the sidebar -->
 <aside class="main-sidebar">
     <!-- sidebar: style can be found in sidebar.less -->
     <section class="sidebar">
          <!-- sidebar menu: : style can be found in sidebar.less -->
         <ul class="sidebar-menu" data-widget="tree">
             <li class="header">MAIN NAVIGATION</li>
         <li class="{{isset ($RealMenu)? "active" : ""}}">
                <a href="{{base_url( "Real" ) }}">
                    <i class="fa fa-dashboard"></i>  
                    <span>Real Time</span>  
                </a>
            </li>
            <li class="{{isset ($DummyMenu)? "active" : ""}}">
                <a href="{{base_url( "Dummy" ) }}">
                    <i class="fa fa-random"></i>  
                    <span>Dummy Data</span>  
                </a>
            </li>        
         </ul>
     </section>
     <!-- /.sidebar -->
 </aside>