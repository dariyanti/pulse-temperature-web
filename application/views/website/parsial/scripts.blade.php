<!-- jQuery 3 -->
<script src="{{ base_url('website/plugins/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ base_url('website/plugins/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ base_url('website/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ base_url('website/plugins/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ base_url('website/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ base_url('website/js/demo.js') }}"></script>
@yield('scripts')
<script>
  
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
